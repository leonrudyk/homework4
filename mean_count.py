import csv


with open('hw.csv', 'r') as f:
    csv_reader = csv.reader(f, delimiter=",")

    csv_list = list(csv_reader)[1:]
    length = len(csv_list)

    # Подсчет среднего арифметического
    avg_height, avg_weight = 0, 0

    for el in csv_list:
        avg_height += float(el[1])
        avg_weight += float(el[2])

    avg_height /= length
    avg_weight /= length
