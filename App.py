from flask import Flask, request
import random
import string
import os

# Импортируем значения avg_height, avg_weight из модуля mean_count (решение с третьего урока)
from mean_count import avg_height, avg_weight


app = Flask(__name__)


@app.route("/")
def main():
    return "123123"


@app.route("/requirements")
def get_requirements():

    # with open("requirements.txt", 'r') as file:
    #     data = file.read()
    #
    # print(data)  # Проверка (вывод в консоль)
    #
    # data = data.replace("\n", "<br>")
    #
    # return data

    # ______________________________

    filename = os.path.join(
        os.path.dirname(__file__),
        'requirements.txt'
    )

    print(filename)

    with open(filename, 'r', encoding='utf-8') as f:
        data = f.read()

    data = data.replace("\n", "<br>")

    return data


@app.route("/random")
def get_random():

    length = request.args.get("length", "10")
    digits = request.args.get("digits", "0")

    # Валидация данных
    try:
        length = int(length)
    except ValueError:
        return "Error. Length is not integer", 400

    if length < 0:
        return "Error. Length cannot be negative", 400

    if digits not in ["0", "1"]:
        return "Error. Digits must be 0 or 1", 400

    chars = string.ascii_lowercase + string.ascii_uppercase

    if digits == "1":
        chars += string.digits

    result = []
    for i in range(length):
        result.append(random.choice(chars))

    return "".join(result)


@app.route("/mean_data")
def get_mean_data():
    return f"Средний рост: {avg_height}<br>Средний вес: {avg_weight}"


app.run()
